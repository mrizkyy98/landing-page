const express = require('express');
const fs = require('fs');
const router = express.Router();
const { body, validationResult } = require('express-validator');
let accounts = fs.readFileSync('auth.json', { encoding: 'utf8' });

router.get("/", (req, res) => {
    res.render("home", { title: "Traditional Games" });
});

router.get("/login", (req, res) => {
    res.render("login", { title: "Login" });
});

router.get("/register", (req, res) => {
    res.render("registration", { title: "Register" });
});

router.get("/games", (req, res) => {
    if (req.session.email) {
        res.render("games", { title: "Rock Paper Scissors", result: '200', message: 'Authorized' });
    } else {
        res.render("games", { title: "Rock Paper Scissors", result: '401', message: 'Unauthorized! User is not logged in' });
    }
});

router.post('/create-user',
    body('name').isString(),
    body('email').isEmail(),
    body('password').isLength({ min: 6 }),
    async (req, res) => {
        let accountEmail;

        const user = {
            name: req.body.name,
            email: req.body.email,
            password: req.body.password,
            confirm: req.body.confirm_password
        };

        const errors = validationResult(req);

        if (!errors.isEmpty() || user.confirm != user.password) {
            return res.status(400).json({ result: '400', message: 'Bad request!' });
        }

        let account = JSON.parse(accounts);

        account.forEach(obj => {
            if (obj.email == user.email) {
                accountEmail = "exist";
            }
        });

        if (accountEmail == "exist") {
            res.status(409).json({ result: '409', message: 'User already exist!' });
        } else {
            account.push({
                name: user.name,
                email: user.email,
                password: user.password
            });

            accounts = JSON.stringify(account);

            fs.writeFileSync("auth.json", accounts, "utf-8");

            res.status(200).json({ result: '200', message: 'Registration Successfully!' });
        }
    });

router.post('/validate-user',
    body('email').isEmail(),
    body('password').isLength({ min: 6 }),
    async (req, res) => {
        if (req.session.email) {
            res.json({ result: '406', message: 'User already logged in.' });
        } else {
            let accountName;

            const user = {
                email: req.body.email,
                password: req.body.password
            };

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({ result: '400', message: 'Bad request!' });
            }

            accounts.forEach(obj => {
                if (obj.email == user.email && obj.password == user.password) {
                    accountName = obj.name;
                }
            });

            if (accountName) {
                req.session.email = req.body.email
                res.status(200).json({ result: '200', message: 'Login successfully!' });
            } else {
                res.status(404).json({ result: '404', message: 'User not found!' });
            }
        }
    });

router.get('/logout', async (req, res) => {
    if (req.session.email) {
        req.session.email = null
        res.json({ result: '200', message: "Logout successfully!" });
    } else {
        res.json({ result: '401', message: 'Unauthorized! User is not logged in' });
    }
});

module.exports = router;