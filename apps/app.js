const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const path = require("path");
const router = require("./router");
const session = require("express-session");
const port = 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.set('trust proxy', 1)
app.use(session({
    secret: 'rizkytest',
    resave: false,
    saveUninitialized: true,
}))

app.set('views', path.join(__dirname, '../views'));
app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, '../views')));
app.use(express.static(path.join(__dirname, '../dist')));
app.use(express.static(path.join(__dirname, '../node_modules')));

app.use(router);

app.listen(port, (req, res) => {
    console.log("Server running");
});