/*
* * Main Class, contains all the game logic
*/
class Games {
    static draw = ["rock", "paper", "scissors"];
    #result = "";

    /*
    * * Constructor, contains manipulation with DOM elements
    */
    constructor() {
        this.textPointPlayer = document.querySelector('.text-point-player');
        this.textPointCom = document.querySelector('.text-point-com');
        this.textStatus = document.querySelector('.text-status-game');
        this.textVersus = document.querySelector('.text-versus');

        this.playerButtons = document.querySelectorAll('.btn-player');
        this.comButtons = document.querySelectorAll('.btn-com');
        this.resetButton = document.querySelector('#btn-reset');

        this.playerButtons.forEach(option => option.addEventListener('click', this.playGame.bind(this)));
        this.resetButton.addEventListener('click', this.resetGame.bind(this));

        this.drawResult = new Result(0, 0, 0);
        this.draw = Games.draw;
    }

    /*
    * * Method playGame, for start the game
    */
    playGame(e) {
        this.drawGame = new Draw(e.target.dataset.option);

        const playerChoice = this.drawGame.playerDraw(),
            comChoice = this.drawGame.comDraw();

        if (
            playerChoice === "rock" && comChoice === "scissors" ||
            playerChoice === "scissors" && comChoice === "paper" ||
            playerChoice === "paper" && comChoice === "rock"
        ) { this.#result = "win"; }

        else if (
            playerChoice === "scissors" && comChoice === "rock" ||
            playerChoice === "paper" && comChoice === "scissors" ||
            playerChoice === "rock" && comChoice === "paper"
        ) { this.#result = "lose"; }

        else {
            this.#result = "draw";
        }

        this.textVersus.classList.add('hidden');
        this.drawResult.showResult(this.textStatus, this.#result);

        [...this.playerButtons].find(element => {
            element.classList.remove('bg-slate-400');
            if (element.dataset.option === playerChoice) {
                element.classList.add('bg-slate-400', 'rounded-lg');
            }
        });

        [...this.comButtons].find(element => {
            element.classList.remove('bg-slate-400');
            if (element.dataset.option === comChoice) {
                element.classList.add('bg-slate-400', 'rounded-lg');
            }
        });

        this.drawResult.countDraw(this.#result);

        this.textPointPlayer.innerHTML = this.drawResult.getCountsDraw().win;
        this.textPointCom.innerHTML = this.drawResult.getCountsDraw().lose;

        if (this.drawResult.getCountsDraw().win === 5) {
            alert("You win!");
            this.resetGame();
        } else if (this.drawResult.getCountsDraw().lose === 5) {
            alert("You lose!");
            this.resetGame();
        }
    }

    /*
    * * Method resetGame, for reset the game
    */
    resetGame(e) {
        this.drawResult.resetCountsDraw();
        this.textPointPlayer.innerHTML = this.drawResult.getCountsDraw().win;
        this.textPointCom.innerHTML = this.drawResult.getCountsDraw().lose;

        this.textStatus.classList.add('hidden');
        this.textVersus.classList.remove('hidden');

        [...this.playerButtons].forEach(element => {
            element.classList.remove('bg-slate-400', 'rounded-lg');
        });

        [...this.comButtons].forEach(element => {
            element.classList.remove('bg-slate-400', 'rounded-lg');
        });
    }
}

/*
* * Class Draw, contains player and computer choice
*/
class Draw {
    constructor(playerChoice) {
        this.playerChoice = playerChoice;
        this.comChoice = Games.draw[Math.floor(Math.random() * Games.draw.length)];
    }

    playerDraw = () => this.playerChoice;
    comDraw = () => this.comChoice;
}


/*
* * [ADDITIONAL]Class Result, contains rock, paper, scissors and counts of wins, loses and draws
*/
class Result {
    constructor(win, draw, lose) {
        this.draws = {
            win: win,
            draw: draw,
            lose: lose,
        }
    }

    /*
    * * Method getCountsDraw, for get the counts of wins, loses and draws, passing from constructor
    */
    getCountsDraw = () => this.draws;

    /*
    * * Method countDraw, for count the wins, loses and draws
    */
    countDraw = (result) => {
        switch (result) {
            case "win":
                this.draws.win++;
                break;
            case "draw":
                this.draws.draw++;
                break;
            case "lose":
                this.draws.lose++;
                break;
        }
    }

    /*
    * * Method resetCountsDraw, for reset the counts of wins, loses and draws
    */
    resetCountsDraw = () => {
        this.draws.win = 0;
        this.draws.draw = 0;
        this.draws.lose = 0;
    }

    /*
    * * Method show resull modal window, for show the result of the game
    */
    showResult = (element, status) => {
        element.classList.remove('hidden');

        if (status === "win") {
            element.innerHTML = "PLAYER 1 WIN!";
        } else if (status === "lose") {
            element.innerHTML = "COM WIN!";
        } else {
            element.innerHTML = "DRAW";
        }
    }
}